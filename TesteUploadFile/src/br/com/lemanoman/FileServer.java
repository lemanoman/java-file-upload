package br.com.lemanoman;

import java.net.*;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

import java.io.*;

public class FileServer {

	/**
	
	public static void main(String[] args) throws IOException {

		boolean hasErrors = false;
		int bytesRead;
		int current = 0;

		ServerSocket serverSocket = null;
		serverSocket = new ServerSocket(13267);

		while (!hasErrors) {
			Socket clientSocket = null;
			System.out.println("THE SERVER STARTED");
			clientSocket = serverSocket.accept();

			InputStream in = clientSocket.getInputStream();

			DataInputStream clientData = new DataInputStream(in);

			String fileName = clientData.readUTF();
			System.out.println("Connection RECEIVED:  "+fileName);
			
			
			OutputStream output = new FileOutputStream(fileName);
			long size = clientData.readLong();
			byte[] buffer = new byte[1024];
			while (size > 0 && (bytesRead = clientData.read(buffer, 0, (int) Math.min(buffer.length, size))) != -1) {
				output.write(buffer, 0, bytesRead);
				size -= bytesRead;
			}

			// Closing the FileOutputStream handle
			in.close();
			clientData.close();
			output.close();
		}
		serverSocket.close();

	}
	
	**/
	
	public static void main(String[] args) throws IOException {

		boolean hasErrors = false;

		ServerSocket serverSocket = null;
		int port = 6050;
		serverSocket = new ServerSocket(6050);

		while (!hasErrors) {
			Socket clientSocket = null;
			System.out.println("THE SERVER STARTED ON "+port);
			clientSocket = serverSocket.accept();

			InputStream in = clientSocket.getInputStream();
			DataInputStream clientData = new DataInputStream(in);
			
			writeToFile(clientData);
			
			in.close();
			clientData.close();
			
		}
		serverSocket.close();

	}
	
	private static void writeToFile(DataInputStream clientData) throws IOException{
		int bytesRead;
		
		ObjectMapper mapper = new ObjectMapper();
		ObjectNode node = mapper.readValue(clientData.readUTF(),ObjectNode.class);
		
		System.out.println("RECEIVED: "+mapper.writeValueAsString(node));
		
		OutputStream output = new FileOutputStream(node.get("name").asText());
		long size = clientData.readLong();
		byte[] buffer = new byte[1024];
		while (size > 0 && (bytesRead = clientData.read(buffer, 0, (int) Math.min(buffer.length, size))) != -1) {
			output.write(buffer, 0, bytesRead);
			size -= bytesRead;
		}
		output.close();
	}
	
}