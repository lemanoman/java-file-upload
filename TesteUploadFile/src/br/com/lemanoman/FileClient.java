package br.com.lemanoman;
import java.net.*;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

import java.io.*;  
   
public class FileClient {  
       
    public static void main(String[] args) throws IOException {  
    	
    	ObjectMapper mapper = new ObjectMapper();
    	ObjectNode node = mapper.createObjectNode();
    	
   
        Socket sock = new Socket("192.168.15.102", 6050);  
   
        //Send file  
        File myFile = new File("C:\\Users\\Kevim Such\\Downloads\\ubuntu-16.04.3-desktop-amd64");  
        byte[] mybytearray = new byte[(int) myFile.length()];  
           
        FileInputStream fis = new FileInputStream(myFile);  
        BufferedInputStream bis = new BufferedInputStream(fis);  
        //bis.read(mybytearray, 0, mybytearray.length);  
           
        DataInputStream dis = new DataInputStream(bis);     
        dis.readFully(mybytearray, 0, mybytearray.length);  
           
        OutputStream os = sock.getOutputStream();  
           
        node.put("name", myFile.getName());
        node.put("lastModified", myFile.lastModified());
        node.put("size", myFile.length());
        
        //Sending file name and file size to the server  
        DataOutputStream dos = new DataOutputStream(os);     
        dos.writeUTF(mapper.writeValueAsString(node));
        dos.writeLong(mybytearray.length);     
        dos.write(mybytearray, 0, mybytearray.length);     
        dos.flush();  
           
        //Sending file data to the server  
        os.write(mybytearray, 0, mybytearray.length);  
        os.flush();  
        
        InputStream in = sock.getInputStream();
        DataInputStream returnDis = new DataInputStream(in);
        
           
        //Closing socket
        os.close();
        dos.close();
        
        in.close();
        returnDis.close();
        sock.close();  
    }  
}